library(stringr)

# função 'arrumar_registro'
#' Tira caracteres não numéricos e imputa '0's à esquerda de um código identificador (cpf, cnpj, etc)
#' Usa a função 'str_pad' do pacote 'stringr'
#' @param x vetor com os códigos identificadores (cpf, cnpj, etc.)
#' @param digitos número de dígitos
#'
#' @return vetor com os códigos sem caracteres não numéricos, com 0's imputados à esquerda para completar
#' o número de dígitos especificados em 'digitos' 
#' @export
#'
#' @examples
arrumar_registro <- function(x,digitos = 11,numeric = T){
  # passo 1: tirar caracteres não numéricos
  y <- gsub("[[:alpha:]]|[[:punct:]]|[[:space:]]","",x)
  
  
  if(!numeric){
    # passo 2: completar com '0' à esquerda se for menor que 'digitos', caso não queira numérico
    z <- str_pad(y,digitos, pad = '0')
  }else{
    # passo3: caso contrário, retorna integer64
    as.integer64(y)
  }
}
#' função 'limpar_valores'
#' 'Limpa' caracteres especiais (como R$) de valores moretários/numéricos, retirando divisor de
#' milhar, fornecido pelo usuário, tratando divisor decimal e convertendo para numérico. Também
#' prevê a inserção de expressão regular informando representação de valor negativo.
#'
#' @param x vetor com os valores monetários e/ou numéricos de qualquer natureza que se quer ajustar
#' @param divmilhar caracter que separa os milhares. Se não houver um, manter como NULL
#' @param dec caracter que separa os decimais. Se não houver um, manter como NULL
#' @param negativo expressão regular que indica a representação de números negativos. 
#' Se não houver, manter NULL
#'
#' @return retorna um vetor numérico após a limpeza e conversão para numérico
#' @export
#'
#' @examples arrumar_valores(c("(R$ 222.395,350)","R$ 195.32","(R$ 20135.8)"), dec = ",",divmilhar = ".",negativo = "^\\(.*\\)$")
limpar_valores <- function(x, 
                            divmilhar = NULL,
                            dec = NULL,
                            negativo = NULL) {
  # passo 1: retirar divisor de milhares, se houver
  if(!is.null(divmilhar)) x <- gsub(divmilhar,"",x, fixed = T)
  
  # passo 2: substituir caracter de decimal por ".", se houver
  if(!is.null(dec)) x <- gsub(dec, ".", x, fixed = T)
  
  # passo 3: detectar expressão regular onde ocorre negativo
  if(!is.null(negativo)) x <- ifelse(grepl(negativo,x),paste0("-",x),x)
  
  # passo 3: tirar caracteres não numéricos
  y <- gsub("[[:alpha:]]|\\$|\\(|\\)|[[:space:]]", "", x)
  
  # passo 4: retornar em formatu numérico
  as.numeric(y)
}

# função 'verifica_cpf'
#' verifica se cpf é válido
#' Usa a função 'strsplit' e str_pad
#' @param x número CPF sem caracteres não numéricos (".","-", etc.)
#' 
#' @return retorna TRUE se o dígito verificador do CPF é válido.
#' @export
#'
#' @examples
verifica_cpf <- function(x){
  x <- str_pad(x,11,pad='0')
  vec_sep <- as.numeric(unlist(strsplit(x,"")))
  verif_dig <- vec_sep[10:11]
  vec_sep <- vec_sep[1:9]
  
  # passo 1: primeiro digito
  y <- sum(vec_sep*(10:2))
  
  #resto
  y <-  y %% 11
  y <- ifelse(y %in% 0:1,0,11-y)
  
  cpf_ok <- y == verif_dig[1]
  
  # passo 2: segundo digito
  vec_sep <- c(vec_sep,y)
  
  z <- sum(vec_sep*(11:2))
  #resto
  z <- z %% 11
  z <- ifelse(z %in% 0:1,0,11-z)
  
  cpf_ok <- cpf_ok & z == verif_dig[2]
  
  return(cpf_ok)
}

### adiciona mes
addmes <- function(x,w){
  y <- Year(x)
  m <- month(x)
  m <- m + w
  y <- y +  trunc((m-1)/12)
  m <- m %% 12
  m <- ifelse(m %in% 0 , 12, m )
  return(as.Date(paste0(y,'-',m,'-',1)))
}

## verifica se há colunas repetidas
ver_col_repetidas <- function(data){
  cols <- names(data)
  tab_cols <- as.data.frame(table(cols))
  tab_cols <- subset(tab_cols, Freq > 1)
  tab_cols$cols
}

# função pega as colunas com padrão p<xx>r<y>, para y = 1,...,k, e empilha, repetindo cada linha k vezes
empilhar_cols_r <- function(cols, data) {
  
  ## Algumas empresas excluem colunas vazias
  num_r_max <- max(as.numeric(gsub("p[0-9]*r([0-9]*).*","\\1",cols)))
  
  p_list <- list()
  for(i in 1:num_r_max){
    # selecionando colunas com padrão pxxxr<i>
    funcao_select <- grep(paste0("r",i,"(_|$)"),cols, value = T)
    p <- select(data,linha,one_of(funcao_select))
    
    # mantendo colunas apenas com padrão pxx
    setnames(p,gsub("r[0-9]{1,2}","",names(p)))
    
    # criando id e inserindo na lista
    id <- as.character(i)
    p_list[[id]] <- p
  }
  
  cols_repetidas <- p_list %>% lapply(ver_col_repetidas) %>% lapply(length) %>% unlist %>% sum
  if(cols_repetidas) stop("colunas repetidas detectadas!")
  
  
  ## redefinindo as colunas com formato igual, levando em conta o "maior" nível
  
  # níveis de formatos e seus scores
  niveis_formatos <- c("logical" = 1,"integer64" = 2,"integer" = 3,"numeric" = 4,"character" = 5)
  
  # listando formatos de cada coluna e inserindo em data.table
  formatos <- lapply(p_list,function(x) lapply(x, class) %>% as.data.table) %>% rbindlist(fill = T)
  
  # pegando o score máximo de formato de cada coluna
  formatos[,c(names(formatos)) := lapply(.SD, function(x) niveis_formatos[x])]
  score_formatos  <- formatos[,lapply(.SD, max,na.rm = T)] %>% as.matrix %>% as.vector
  
  # associando a cada coluna formato que deve ter
  cols_formt <- niveis_formatos %>% names %>% .[score_formatos]
  names(cols_formt) <- names(formatos)
  
  # aplicando os formatos
  p_list <- lapply(p_list,function(x){
    cols_formt_x <- names(cols_formt) %>% intersect(names(x))
    cols_formt_x <- cols_formt[cols_formt_x]
    cmd <- paste0(names(cols_formt_x),
                  " = as.",
                  cols_formt_x,
                  "(",
                  names(cols_formt_x),
                  ")",
                  collapse = ", \n")
    cmd <- paste0("`:=`(",cmd,")")
    x[,eval(parse(text = cmd))]
  })
  
  rbindlist(p_list,idcol = "r", fill = T)
}


struct_dummies <- function(data,cols,commom) {
  # observation column
  obs_col <- grep("observ",cols,value = T)
  cols_melt <- cols %>% setdiff(obs_col)
  if(length(obs_col) == 0) obs_col = NULL
  
  # melting data
  p <- melt(data,
            id.vars = c("linha",obs_col),
            variable.name = commom,
            value.name = "class",
            measure.vars = cols_melt
  )
  
  # filter with no NA
  p <- p[!is.na(class),]
  
  # cleaning descriptions
  p[, `:=`(class_clean =
             commom %>%
             paste0("_",class) %>%
             str_trim(side = "both") %>%
             estruturar_string,
           value = 1)]
  # p[,class_clean %>% unique %>% sort]
  
  # new filter where class_clean equals 'commom' variable only
  p <- p[class_clean != commom,]
  
  # pattern on observation col
  if(!is.null(obs_col)){
    obs_col_pattern <- paste0(commom,"_obs")
    setnames(p,obs_col,obs_col_pattern)
    } else{
      obs_col_pattern <- NULL
      }
  
  # casting
  formula <- 
    paste0(
      paste(c("linha",obs_col_pattern),collapse = " + "),
      " ~ class_clean"
    )
  p_cast <- dcast(p,
                  eval(parse(text = formula)),
                  value.var = "value",
                  fill = 0)
  
  # merging with original data
  p_cast <- p_cast[(select(data,linha)),on = "linha"]
  p_cast[,paste0(commom,"_others") := character(0)]
  
  # output: database and list of occurrences
  return(list(database = p_cast,
              occurrences = p[,list(n_cases = .N,
                                    lines = linha %>% paste0(collapse = "/"),
                                    original_classes = class %>% unique %>% paste0(collapse = "/")),
                              by = c("class_clean")] %>%
                setorder(class_clean,-n_cases)))
}





############################################
## Merge power


merge2wayOR <- function(d1,d2,idA,idB){
  # create index column in both data.tables
  d1[, idx1 := .I]
  d2[, idx2 := .I]
  
  mAB <- c(idA, idB)
  mA <- idA
  mB <- idB
  
  # inner join on idA and idB
  j1 <- d1[d2, .(idx1, idx2), on = mAB , nomatch = 0L]
  m1 <- unique(j1$idx1)
  m2 <- unique(j1$idx2)
  print('a')
  
  # inner join on idA
  j2 <- d1[!(idx1 %in% m1)][d2[!(idx2 %in% m2)], .(idx1, idx2), on = mA, nomatch = 0L]
  m1 <- append(m1, unique(j2$idx1))
  m2 <- append(m2, unique(j2$idx2))
  print('B')
  # inner join on idB
  j3 <- d1[!(idx1 %in% m1)][d2[!(idx2 %in% m2)], .(idx1, idx2), on = mB, nomatch = 0L]
  m1 <- append(m1, unique(j3$idx1))
  m2 <- append(m2, unique(j3$idx2))
  print('C')
  
  # combine results
  j4 <- rbindlist(
    list(
      AB = cbind(
        d1[idx1 %in% j1[, idx1]],
        d2[idx2 %in% j1[, idx2]]),
      A. = cbind(
        d1[idx1 %in% j2[, idx1]],
        d2[idx2 %in% j2[, idx2]]),
      .B = cbind(
        d1[idx1 %in% j3[, idx1]],
        d2[idx2 %in% j3[, idx2]])),
    fill = TRUE, 
    idcol = "match_on") %>% return

}



merge2wayOR_full <- function(d1,d2,idA,idB){
  # create index column in both data.tables
  d1[, idx1 := .I]
  d2[, idx2 := .I]
  
  mAB <- c(idA, idB)
  mA <- idA
  mB <- idB
  
  # inner join on idA and idB
  j1 <- d1[d2, .(idx1, idx2), on = mAB , nomatch = 0L]
  m1 <- unique(j1$idx1)
  m2 <- unique(j1$idx2)
  print('a')
  
  # inner join on idA
  j2 <- d1[!(idx1 %in% m1)][d2[!(idx2 %in% m2)], .(idx1, idx2), on = mA, nomatch = 0L]
  m1 <- append(m1, unique(j2$idx1))
  m2 <- append(m2, unique(j2$idx2))
  print('B')
  # inner join on idB
  j3 <- d1[!(idx1 %in% m1)][d2[!(idx2 %in% m2)], .(idx1, idx2), on = mB, nomatch = 0L]
  m1 <- append(m1, unique(j3$idx1))
  m2 <- append(m2, unique(j3$idx2))
  print('C')
  
  ##name ajust
  
  intersect_names <- intersect(names(d1),names(d2))
  setnames(d1,intersect_names,paste0(intersect_names,"_d1"))
  setnames(d2,intersect_names,paste0(intersect_names,"_d2"))
  
  # combine results
  rbindlist(
    list(
      AB = cbind(
        d1[idx1 %in% j1[, idx1]],
        d2[idx2 %in% j1[, idx2]]),
      A. = cbind(
        d1[idx1 %in% j2[, idx1]],
        d2[idx2 %in% j2[, idx2]]),
      .B = cbind(
        d1[idx1 %in% j3[, idx1]],
        d2[idx2 %in% j3[, idx2]]),
      d1 = cbind(
        d1[!(idx1 %in% m1)],
        d2[1,lapply(.SD,function(x) NA)]),
      d2 = cbind(
        d1[1,lapply(.SD,function(x) NA)],
        d2[!(idx2 %in% m2)])),
    fill = TRUE, 
    idcol = "match_on") %>% return
  
}



#' Title estruturar_string
#' Retorna um vetor de novos nomes estruturados, sem repetição, minúsculos, sem caracter especial e com "_" no lugar de "."
#'
#' @param x vetor de strings
#' @param encoding encogin da base, sendo na maiorria 'utf8', 'latin1' OU 'Windows-1252
#'
#' @return vetor com novos nomes padronizados
#' @export
#'
#' @examples
estruturar_string <- function(x, encoding = "utf8") {
  x %>%
    iconv(from = encoding, to = "ASCII//TRANSLIT") %>% # tira caracteres de a acentuação
    tolower %>% ## tudo em minúsculo
    gsub(pattern = "[[:space:]]{1,}", replacement = "_") %>% # substituindo 1 ou mais espaços por "_"
    gsub(pattern = "\\.", replacement = "_") %>% # substituindo "." por "_"
    gsub(pattern = "\\-", replacement = "_") %>% # substituindo "-" por "_"
    gsub(pattern = "\\(|\\)",replacement = "")  %>% # tirando parêntesis
    gsub(pattern = "([[:digit:]]{1,})$", replacement = "") %>% # tirando números no final
    gsub(pattern = "_{2,}",replacement = "_") %>% # tirando "_" múltilpos
    gsub(pattern = "([[:punct:]]{1,})$", replacement = "") # tirando pontuação no final
    
}

#' Title estruturar_nomes
#' Retorna um vetor de novos nomes estruturados, sem repetição, minúsculos, sem caracter especial e com "_" no lugar de "."
#'
#' @param data data.frame ou data.table 
#' @param encoding encogin da base, sendo na maiorria 'utf8', 'latin1' OU 'Windows-1252
#'
#' @return vetor com novos nomes padronizados
#' @export
#'
#' @examples
estruturar_nomes <- function(data, encoding = "utf8"){
  colunas_validas <- data %>%
    names %>%
    estruturar_string(encoding) %>% # pré-estrutura tirando acento, ".", espaço, etc.
    make.names(unique = T) %>% # repetidos são concatenados a números
    gsub(pattern = "\\.", replacement = "_") %>% # substituindo "." por "_"
    gsub(pattern = "\\-", replacement = "_") %>% # substituindo "-" por "_"
    gsub(pattern = "_{2,}",replacement = "_") %>% # tirando "_" múltilpos
    gsub(pattern = "(_|[[:punct:]])$",replacement = "") # tirando pontuação no final 
  
  ## retorna colunas estruturadas
  colunas_validas
}



#' Title função 'formatar_num': acrescenta big.mark = "." e decimal.mark = ",", de acordo com o número de dígitos escolhido.
#' função 'formatar_num': acrescenta big.mark = "." e decimal.mark = ",", de acordo com o número de dígitos escolhido.
#' @param x vetor ou número
#' @param digitos número de dígitos que irão aparecer
#'
#' @return
#' @export
#'
#' @examples
formatar_num <- function(x,digitos){
  formatC(x,
          big.mark = ".",
          decimal.mark = ",",
          format = "f",
          digits = digitos)
}

n_aas <- function (N, e, z, v = 1/4,prop = T, aloc = NULL) {
  N/((e/z)^2*(N - prop)/v + 1)
}

n_aae <- function (N, e, z, v = 1/4,prop = NULL, aloc = "unif") {
  # pesos
  wh_pop  <- N/sum(N)
  if(aloc == "unif"){
    wh_samp <- rep(1/length(N),length(N))  
  }else{
    wh_samp <- wh_pop
  }
  
  # comp. fração
  n_num <- ifelse(aloc == "otima",
                  sum(sqrt(v)*wh_pop)^2,
                  sum(v*(wh_pop^2)/wh_samp)
                  )
  n_den <- (e/z)^2 + sum(wh_pop*v)/sum(N)
  
  # tamanho total
  n_tot <- n_num/n_den
  
  # tamanho por estrato
  n_tot*wh_samp
}

n_aac <- function(N, e, z, v, prop = F, aloc = "unif"){
  N/((e/z)^2*(N - prop)/v + 1)
}


find_lat_long <- function(addr) {
  url = paste('http://maps.google.com/maps/api/geocode/xml?address=', 
              addr,'&sensor=false',sep='') 
  
  doc = xmlTreeParse(url) 
  root = xmlRoot(doc) 
  lat = xmlValue(root[['result']][['geometry']][['location']][['lat']]) 
  long = xmlValue(root[['result']][['geometry']][['location']][['lng']])
  silent = T
  data.table(endereco = addr,lat,long)
}
  
find_lat_long_ex <- function(addr){
  result <- try(find_lat_long(addr),silent = T)
  if(inherits(result,"try-error")){
    data.table(endereco = addr,lat = NA, long = NA)
  }else{
    result
  }
}



lista_e <- function(x,connective = " and "){
  x <- unique(x)
  if(length(x) > 1){
    y <- paste(head(x,-1),collapse = ", ")
    z <- tail(x,1)
    paste(y,z,sep = connective)
  }else{
    x
  }
}

prob_selec_ssu <- function(n,p,f2){
  seq_n <- 0:n
  exp_n <- (1-f2)^seq_n
  bin_n <- dbinom(seq_n,n,p)
  1 - sum(exp_n*bin_n)
}


