### packages  --------------------------  --------------------------  -------------------------- 

rm(list = ls());gc() # cleaning memory

library(dplyr)
library(data.table)
library(openxlsx)
library(ggplot2)
source("metodos.r")

### globals  --------------------------  --------------------------  -------------------------- 

# loading data
sp_data.dt <-
  read.xlsx(xlsxFile = "data/Mapping Social Protection Programmes.xlsx",
            sheet = 1,
            na.strings = c("NA",""," ","N.A"," NA")) %>%
  as.data.table

# pre_structuring columns
sp_data.dt %>% setnames(sp_data.dt %>% estruturar_nomes())

# column "linha" as the line id
sp_data.dt[,linha := .I]

# function to join with 'sp_data.dt' original data
merge_original <- function(){
  if(exists("sp_data_new.dt",envir = .GlobalEnv)){
    sp_data_new.dt <<- sp_data_new.dt[prov,on = "linha"]
  }else{
    sp_data_new.dt <<- prov
  }
  rm(prov, envir = .GlobalEnv)
}


### structuring dummies --------------------------  --------------------------  -------------------------- 

## > structuring dummies for programme_typology ----

# list of cols
typology_cols <- grep("programme_typology", names(sp_data.dt), value = T)

# structuring
str_elements <- struct_dummies(sp_data.dt,
                               cols = typology_cols,
                               commom = "pt")
prov <- str_elements$database

# merge withg original
merge_original()

# saving occurences of each class
write.csv2(str_elements$occurrences,
           "data/classes_programme_typology.csv",
           row.names = F)


## > structuring dummies for type_of_benefits ----

# list of cols
type_of_benefits_cols <- grep("type_of_benefits", names(sp_data.dt), value = T)

# structuring
str_elements <- struct_dummies(sp_data.dt,
                               cols = type_of_benefits_cols,
                               commom = "tb")
prov <- str_elements$database

# merge withg original
merge_original()

# saving occurences of each class
write.csv2(str_elements$occurrences,
           "data/classes_type_of_benefits.csv",
           row.names = F)

## > structuring dummies for targeting_mechanism ----

# changing "target_group_and_targeting_mechanisms" to "observations_targeting_mechanism"
if("target_group_and_targeting_mechanisms" %in% names(sp_data.dt)){
  setnames(sp_data.dt,
           "target_group_and_targeting_mechanisms",
           "observations_targeting_mechanism")}

# list of cols
targeting_mechanism_cols <- grep("targeting_mechanism", names(sp_data.dt), value = T)

# structuring
str_elements <- struct_dummies(sp_data.dt,
                               cols = targeting_mechanism_cols,
                               commom = "tm")
prov <- str_elements$database

# merge withg original
merge_original()

# saving occurences of each class
write.csv2(str_elements$occurrences,
           "data/classes_targeting_mechanism.csv",
           row.names = F)


## > structuring dummies for target_group ----

# list of cols
target_group_cols <- grep("target_group", names(sp_data.dt), value = T)

if(grepl("mechanisms|delete",target_group_cols) %>% any){
  target_group_cols <- 
    target_group_cols %>%
    setdiff(
      grep(
        "mechanisms|delete",
        target_group_cols,
        value = T
      )
    )
}
  

# structuring
str_elements <- struct_dummies(sp_data.dt,
                               cols = target_group_cols,
                               commom = "tg")
prov <- str_elements$database

# merge withg original
merge_original()

# saving occurences of each class
write.csv2(str_elements$occurrences,
           "data/classes_target_group.csv",
           row.names = F)


## > structuring dummies for child specif area ----

# list of cols
child_area_cols <- grep("child_specific", names(sp_data.dt), value = T)

# structuring
str_elements <- struct_dummies(sp_data.dt,
                               cols = child_area_cols,
                               commom = "chi")
prov <- str_elements$database

# merge withg original
merge_original()

# saving occurences of each class
write.csv2(str_elements$occurrences,
           "data/classes_child_area.csv",
           row.names = F)


### structuring numeric data --------------------------  --------------------------  -------------------------- 

## > structuring values of programme coverage ----

# columns abaout program coverage
cols_coverage <- grep("coverage",names(sp_data.dt),value = T)
cols_coverage_obs <- grep("obser",cols_coverage,value = T)
cols_coverage <- setdiff(cols_coverage,cols_coverage_obs)

# cols with cleaned values of coverage
cols_coverage_clean <- paste0(cols_coverage,"_clean")

# cleaning values
sp_data.dt[,c(cols_coverage_clean) := lapply(.SD,function(x){
  x %>%
    gsub(pattern = "\\,|\\.", replace = "") %>% # removing puncts and commas
    as.numeric() # convergin to numeric
  }),
  .SDcols = cols_coverage]

# selecting variables
prov <- sp_data.dt %>% select(linha,
                              one_of(c(cols_coverage_clean,cols_coverage_obs)))

# merge withg original
merge_original()

## > structuring values of minimum/maximum amount of benefits ----

# columns abaout program amount
cols_amount <- grep("amount",names(sp_data.dt),value = T)
cols_amount_obs <- grep("obser",cols_amount,value = T)
cols_amount <- setdiff(cols_amount,cols_amount_obs)

# cols with cleaned values of amount
cols_amount_clean <- paste0(cols_amount,"_clean")


# cleaning values
sp_data.dt[,c(cols_amount_clean) := lapply(.SD,function(x){
  x %>%
    str_extract(pattern = "([[:digit:]]|[[:punct:]]){1,}") %>% # removing character values
    gsub(pattern = "\\,", replace = "") %>% # removing commas
    as.numeric() # convergin to numeric
  }),
  .SDcols = cols_amount]

# listing lost data
amount_lost <- sp_data.dt[!is.na(minimum_amount_of_benefits_in_us_ppp) & 
                            is.na(minimum_amount_of_benefits_in_us_ppp_clean),
                          list(linha,
                               minimum_amount_of_benefits_in_us_ppp,
                               minimum_amount_of_benefits_in_us_ppp_clean)]

# selecting variables
prov <- sp_data.dt %>% select(linha,
                              one_of(c(cols_amount_clean,cols_amount_obs)))

# merge withg original
merge_original()

# saving lost values
write.csv2(amount_lost,
           "data/amount_lost_cleanning.csv",
           row.names = F)


### output --------------------------  --------------------------  --------------------------

# programmes and country id's
cols_benefit <- sp_data.dt %>% names %>% grep(pattern = "delivery",value = T)
sp_data_new.dt <- (sp_data.dt %>%
            select(linha,
                   country_name_procv,
                   country_code,
                   programme,
                   programme_code,
                   sub_programme,
                   sub_programme_code,
                   start_date_year,
                   one_of(cols_benefit)))[sp_data_new.dt, on = "linha"]

setnames(sp_data_new.dt,
         "country_name_procv",
         "country")

write.csv2(sp_data_new.dt,
           "data/mapping_social_protection_programmes_NEW_STRUCTURE.csv",
           row.names = F)

### some analysis --------------------------  --------------------------  -------------------------- 


